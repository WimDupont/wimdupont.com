Installation steps with information taken from the https://wiki.archlinux.org/title/Installation_guide[official installation guide] to assist the process with encryption. Be aware
that this guide may become outdated and/or contain bugs. Also, this guide only covers a specific installation, so no swapfile, only with EFI system partition, etc. 

== Setup

. https://archlinux.org/download[Download ISO file]

. Verify signature
+
--
[source,bash]
----
$ gpg --keyserver-options auto-key-retrieve --verify archlinux-version-x86_64.iso.sig
----
or from existing arch installation:
[source,bash]
----
$ pacman-key -v archlinux-version-x86_64.iso.sig
----
--
. Prepare USB flash drive
+
--
[source,bash]
----
$ dd bs=4M if=path/to/archlinux-version-x86_64.iso of=/dev/x conv=fsync oflag=direct status=progress
----
--

. Boot from USB file (UEFI)

== Installation

. Verify boot mode
+
--
[source,bash]
----
$ ls /sys/firmware/efi/efivars
----
If the command shows directory without error: booted in UEFI mode.
--
. Connect to the internet
+
--
[source,bash]
----
$ ip link
$ ping archlinux.org
----
--
. Update the system clock
+
--
[source,bash]
----
$ timedatectl status
----
--
. Partition the disks
+
--
[source,bash]
----
$ fdisk -l
or
$ lsblk
$ fdisk /dev/the_disk_to_be_partitioned
----
--
.. Delete all existing partitions
.. Create 2 new primary partitions on main disk
.. Defaults except first partition endsize, type: +600M
.. Optional: secure erase:
+
--
[source,bash]
----
$ dd if=/dev/urandom of=/dev/sdX# bs=4096 status=progress
----
--
. Encrypt main partition (and others if multiple disks)
+
--
[source,bash]
----
$ cryptsetup luksFormat /dev/sdX#
----
--
. Open encrypted partition (name is needed for reference but not permanent)
+
--
[source,bash]
----
$ cryptsetup open /dev/sdX# {name}
----
--
. Format the partitions
+
--
[source,bash]
----
$ mkfs.fat -F 32 /dev/boot_partition
$ mkfs.ext4 /dev/mapper/{name}
----
--
. Mount the file systems
+
--
[source,bash]
----
$ mount /dev/mapper/{name} /mnt
$ mkdir /mnt/boot
$ mount /dev/boot_partition /mnt/boot
----
--
. Optional: sort mirrors on geographical location
+
--
[source,bash]
----
$  vim /etc/pacman.d/mirrorlist
----
--
. Install (essential) packages
+
--
[source,bash]
----
$ pacstrap -K /mnt base base-devel linux linux-firmware grub networkmanager cryptsetup lvm2 vim
----
--
. Generate fstab (define how partitions should be mounted)
+
--
[source,bash]
----
$ genfstab -U /mnt >> /mnt/etc/fstab
----
--
. Change root into new system
+
--
[source,bash]
----
$ arch-chroot /mnt
----
--
. Time zone (region and city variables)
+
--
[source,bash]
----
$ ln -sf /usr/share/zoneinfo/{Region}/{City} /etc/localtime
$ hwclock --systohc
----
--
. Localization
+
--
[source,bash]
----
$ vim /etc/locale.gen
----
--
.. uncomment lines
+
--
[source,conf]
----
en_US.UTF-8 UTF-8
en_US ISO-8859-1
----
[source,bash]
----
$ locale-gen
----
--
.. set lang variable
+
--
[source,bash]
----
$ vim /etc/locale.conf
----
--
... add text
+
--
[source,conf]
----
LANG=en_US.UTF-8
----
--
. Network configuration
+
--
[source,bash]
----
$ vim /etc/hostname
----
--
.. add text
+
--
[source,conf]
----
myhostname
----
--
.. Config hosts
+
--
[source,bash]
----
$ vim /etc/hosts
----
--
.. add text (myhostname is variable based on /etc/hostname)
+
--
[source,conf]
----
127.0.0.1	localhost
::1		localhost
127.0.1.1	{myhostname}.localdomain {myhostname}
----
--
.. Enable services
+
--
[source,bash]
----
$ systemctl enable NetworkManager.service
$ systemctl enable systemd-resolved.service
----
--
. Passwords and users
+
--
[source,bash]
----
$ passwd
$ useradd -G wheel -m {user}
$ passwd {user}
----
--
. Initial ramdisk
+
--
[source,bash]
----
$ vim /etc/mkinitcpio.conf
----
--
.. Find like that starts with: HOOKS(base udev...) and add near the end but still
inside the brackets:
+
--
[source,conf]
----
encrypt lvm2
----
--
.. Create new initramfs
+
--
[source,bash]
----
$ mkinitcpio -P
----
--
. Boot loader (GRUB)
.. Exit chroot environment by typing _exit_ or pressing _Ctr+d_.
.. Add partition information to grub file
+
--
[source,bash]
----
$ lsblk -f >> /mnt/etc/default/grub
----
--
. Chroot into system and edit grub file
+
--
[source,bash]
----
$ arch-chroot /mnt
$ vim /etc/default/grub
----
--
.. Grab output of the previous (lsbblk -f) command at the bottom and move it to the
top.
.. Comment it
.. Add to the GRUB_CMDLINE_LINUX_DEFAULT property to look like this (where {brackets} should be replaced with correct UUID and "cryptname" to preferred name)
+
--
[source,conf]
----
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet cryptdevice=UUID={uuid-of-/dev/sdX#}:cryptname root=UUID={uuid-of-/dev/mapper/{name}}"
----
--
. Install GRUB
+
--
[source,bash]
----
$ pacman -S efibootmgr
$ grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
----
--
. Microcode (CPU updates), depending on processor manufacturer run following
+
--
[source,bash]
----
$ pacman -S amd-ucode
$ pacman -S intel-ucode
----
--
. Generate grub cfg
+
--
[source,bash]
----
$ grub-mkconfig -o /boot/grub/grub.cfg
----
--
. Exit chroot, reboot, remove USB, and (hopefully) enjoy.
