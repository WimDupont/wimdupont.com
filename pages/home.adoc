:figure-caption!:

[.subheader]
Welcome to my webpage

image::files/images/wim.jpg[alt="Photo of myself"]

Hi, I am Wim. I like Web 1.0, so here is my website.

I try to keep the site and its generation as simple as possible. It is all static web pages and I do not use
javascript. This also gives me a great excuse for my lack of design skills. The site is mainly built using
AsciiDoc (which might change) and a bash script. (link:https://git.wimdupont.com/wimdupont.com/files[source code])
