#!/bin/bash

readonly NGINX_DIR=/usr/share/nginx/html
readonly BLOG_DEST_DIR=$NGINX_DIR/blog
readonly GUIDES_DEST_DIR=$NGINX_DIR/guides
readonly SOFTWARE_DEST_DIR=$NGINX_DIR/software
readonly RSS_DEST_FILE=$NGINX_DIR/rss.xml

readonly REPO_PATH=$(echo "$(dirname -- "$(readlink -f "${BASH_SOURCE}")")")
readonly PAGES_REPO_DIR=$REPO_PATH/pages
readonly BLOG_REPO_DIR=$PAGES_REPO_DIR/blog
readonly GUIDE_REPO_DIR=$PAGES_REPO_DIR/guides
readonly SOFTWARE_REPO_DIR=$PAGES_REPO_DIR/software
readonly ERROR_REPO_DIR=$PAGES_REPO_DIR/error
readonly BOOKLIST_FILE=$REPO_PATH/content/booklist.csv
readonly LINKS_FILE=$REPO_PATH/content/links.csv
readonly HEADER_FILE=$PAGES_REPO_DIR/header/header.adoc
readonly FOOTER_FILE=$PAGES_REPO_DIR/footer/footer.adoc

rm -rf $NGINX_DIR/*

mkdir -p $NGINX_DIR/files
mkdir -p $NGINX_DIR/stylesheets
mkdir -p $BLOG_DEST_DIR
mkdir -p $GUIDES_DEST_DIR
mkdir -p $SOFTWARE_DEST_DIR

cp -rf $REPO_PATH/images "$NGINX_DIR/files"
cp $REPO_PATH/pubkey.gpg "$NGINX_DIR/files"
cp $REPO_PATH/main.css $NGINX_DIR/stylesheets
cp $REPO_PATH/robots.txt "$NGINX_DIR/"

get_page_header() {
	declare -r TITLE="${1^}"
	declare -r ROOT_NAV=$(echo $2 | sed 's/\//\\\//g')
	declare -r SUB_TITLE=$3

	cat $HEADER_FILE | sed "s/{root_nav}/${ROOT_NAV}/g" | sed "s/{title}/${TITLE}/g" && test "$SUB_TITLE" == 1 && echo -e "[.subheader]\n--\n${TITLE}\n-- "
}

get_page_footer() {
	declare -r ROOT_NAV=$(echo $1 | sed 's/\//\\\//g')

	cat $FOOTER_FILE | sed "s/{root_nav}/${ROOT_NAV}/g"
}

generate_pages() {
	repo_path=$1
	dest_path=$2
	root_nav=$3
	sub_title=$4

	for file in $repo_path/* ; do
		if [[ -f $file ]]; then
			cd ${repo_path}
			date=$(git log -1 --pretty="format:%ci" "$file")

			cd ${dest_path}
			filename=$(basename "${file}" .adoc)
			get_page_header "$filename" "$root_nav" "$sub_title" >> "$dest_path/$filename.adoc"
			cat "$file" >> "$dest_path/$filename.adoc"
			get_page_footer "$root_nav" >> "$dest_path/$filename.adoc"

			asciidoctor "$dest_path/$filename.adoc"
			rm "$dest_path/$filename.adoc"

			touch -a -m -d "${date}" "$dest_path/$filename.html"
			touch -a -m -d "${date}" "$repo_path/$filename.adoc"
		fi
	done	
}

as_href() {
	dir_ref=$1
	filename=$2
	date=$3
	filelink=$(echo "$filename" | sed 's/ /%20/g')

	echo -n "link:$dir_ref/$filelink[$filename]" && test -n "$date" && echo -n " _- ${date}_"
}

generate_href_page() {
	name=$1
	title=$2
	ref_dir=$3
	with_date=$4

	cd $ref_dir
	get_page_header "${name}" > "$NGINX_DIR/$name.adoc"

	test -n "${title}" && echo -e "[.subheader]\n${title}\n" >> "$NGINX_DIR/$name.adoc"

	readarray -t files < <(stat -c '%Y %n' * | sort -n -r)

	for file in "${files[@]#* }" ; do
		filename=$(echo "${file##*/}" | sed 's/.html//')
		if [[ $with_date -eq 1 ]]; then
			date=$(date -r "$file" "+%Y-%m-%d")
			art_fileref=$(as_href "$name" "$filename" "$date")
		else
			art_fileref=$(as_href "$name" "$filename")
		fi
		echo "* $art_fileref" >> "$NGINX_DIR/$name.adoc"
	done
	cd $NGINX_DIR
	get_page_footer "" >> "$NGINX_DIR/$name.adoc"
	asciidoctor "$name.adoc"
	rm "$name.adoc"
}

generate_error_pages() {
	for file in "$ERROR_REPO_DIR"/* ; do
		cp $file "$NGINX_DIR/"
		cd $NGINX_DIR
		filename=$(basename "${file}" .adoc)
		htmlfile=$(echo "$file" | sed 's/.adoc$/.html/')
		asciidoctor $filename.adoc
		rm "$filename.adoc"
	done
}

generate_books_page() {
	declare -r DEST_FILE=/usr/share/nginx/html/books.adoc

	get_page_header "Books" > $DEST_FILE
	echo -e "[.subheader]\nThis is the collection of books of which I own a physical copy\n" >> $DEST_FILE
	
	sort -t';' -k4,4 -k2,2 -k5,5 -k6,6 -o $BOOKLIST_FILE $BOOKLIST_FILE
	
	category_check=
	while IFS=';' read title author isbn category series series_number; do
		if [[ $category_check != $category ]]; then
			test -n "$category_check" && echo "|===" >> $DEST_FILE
			echo -e "\n$category\n\n[cols=\"<70%,>30%\"]\n|===" >> $DEST_FILE
			category_check=$category
		fi
		if [[ -z "$series" ]]; then
			echo "|link:https://openlibrary.org/isbn/$isbn[$title]|$author" >> $DEST_FILE
		else
			series=$(test -z $series_number && echo -n $series || echo -n "$series #$series_number")
			echo "|link:https://openlibrary.org/isbn/$isbn[$title] [.series]#($series)#|$author" >> $DEST_FILE
		fi
	done < "$BOOKLIST_FILE"

	echo "|===" >> $DEST_FILE
	cd $NGINX_DIR
	get_page_footer >> $DEST_FILE
	asciidoctor $DEST_FILE
	rm $DEST_FILE
}

generate_links_page() {
	declare -r DEST_FILE=/usr/share/nginx/html/links.adoc

	get_page_header "Links" > $DEST_FILE
	echo -e "[.subheader]\nLinks to websites on various topics\n" >> $DEST_FILE

	sort -t';' -k4,4 -k1,1 -o $LINKS_FILE $LINKS_FILE
	
	category_check='1'
	while IFS=';' read title url description category image ; do
		if [[ $category_check != "$category" ]]; then
			test -n "$category" && echo -e "\n== $category\n" >> $DEST_FILE
			category_check=$category
		fi
		echo "* link:$url[$title] - $description" >> $DEST_FILE

	done < "$LINKS_FILE"
	cd $NGINX_DIR
	get_page_footer >> $DEST_FILE
	asciidoctor $DEST_FILE
	rm $DEST_FILE
}

generate_rss() {
	cd $BLOG_REPO_DIR
	echo '<rss version="2.0"><channel><title>Wim Dupont - Blog</title><link>https://wimdupont.com/blog</link><language>en</language>' > $RSS_DEST_FILE
	readarray -t files < <(stat -c '%Y %n' * | sort -n -r)

	for file in "${files[@]#* }" ; do
		filename=$(basename "${file}" .adoc)
		date=$(stat -c '%y' "$file")

		asciidoctor -e "$file"
		content=$(cat "${filename}.html" | sed 's/</\&lt;/g')

		rm "${filename}.html"

		echo "<item>
<title>$filename</title>
<description>$content</description>
<link>https://wimdupont.com/blog/$filename</link>
<author>Wim Dupont</author>
<pubDate>$date</pubDate>
<guid>$filename-$date</guid>
</item>" >> $RSS_DEST_FILE
	done
	echo '</channel></rss>' >> $RSS_DEST_FILE
}

generate_error_pages

generate_books_page
generate_links_page

generate_pages $PAGES_REPO_DIR $NGINX_DIR
generate_pages $GUIDE_REPO_DIR $GUIDES_DEST_DIR "../" 1
generate_pages $SOFTWARE_REPO_DIR $SOFTWARE_DEST_DIR "../" 1
generate_pages $BLOG_REPO_DIR $BLOG_DEST_DIR "../" 1

generate_href_page "guides" "" $GUIDES_DEST_DIR
generate_href_page "software" "Shameless advertisement of some of my easy hackable software" $SOFTWARE_DEST_DIR
generate_href_page "blog" "" $BLOG_DEST_DIR 1

generate_rss
